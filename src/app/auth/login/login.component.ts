import { AuthController } from './../auth-controller';
import { Component, Injector, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
// import pq from 'pqgrid';
// import * as $ from 'jquery';
declare var $: any;
@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent extends AuthController implements OnInit {
  loginForm :any = FormGroup;

  heading=[
    { 'label': 'age' },
    { 'label': 'name' },
    { 'label': 'address' },
    { 'label': 'email' },
  ];

  data=[
    {'name':'Sujan', 'age': '21', 'address':'bkt'},
    {'name':'Sanjay', 'age': '21', 'address':'bkt', 'email':'sujan@gmail.com'},
    {'name':'Sujan', 'age': '21', 'address':'bkt', 'email':'sujan@gmail.com'},
    {'name':'Sujan', 'age': '21', 'address':'bkt', 'email':'sujan@gmail.com'},

  ];

  test = [
    {
      'header':[
        'name', 'age', 'address', 'email'
      ],

      'data':[
        {'name':'Sujan', 'age': '21', 'address':'bkt'},
    {'name':'Sanjay', 'age': '21', 'address':'bkt', 'email':'sujan@gmail.com'},
    {'name':'Sujan', 'age': '21', 'address':'bkt', 'email':'sujan@gmail.com'},
    {'name':'Sujan', 'age': '21', 'address':'bkt', 'email':'sujan@gmail.com'},
      ]
      
    }
  ]

  public columns;
  public rowData;

  constructor(
    public formBuilder: FormBuilder,
    public injector : Injector
  ) { 
    super(injector);
  }

  ngOnInit(): void {
    this.rowData = [
      { rank: 1, 
        company: "Exxon Mobil", 
        revenues: 339938.0, 
        profits: 36130.0 
      },
      {
        rank: 2,
        company: "Wal-Mart Stores",
        revenues: 315654.0,
        profits: 11231.0
      },
      {
        rank: 3,
        company: "Royal Dutch Shell",
        revenues: 306731.0,
        profits: 25311.0
      },
      { rank: 4, 
        company: "BP", 
        revenues: 267600.0, 
        profits: 22341.0 
      },
      {
        rank: 5,
        company: "General Motors",
        revenues: 192604.0,
        profits: -10567.0
      },
      { rank: 6, 
        company: "Chevron", 
        revenues: 189481.0, 
        profits: 14099.0 
      },
      {
        rank: 7,
        company: "DaimlerChrysler",
        revenues: 186106.3,
        profits: 3536.3
      },
      { rank: 8, 
            company: "Toyota Motor", 
            revenues: 185805.0, 
            profits: 12119.6 
          },
      { rank: 9, 
            company: "Ford Motor", 
            revenues: 177210.0, 
            profits: 2024.0 
          },
      {
        rank: 10,
        company: "ConocoPhillips",
        revenues: 166683.0,
        profits: 13529.0
      },
      {
        rank: 11,
        company: "General Electric",
        revenues: 157153.0,
        profits: 16353.0
      },
      { rank: 12, 
            company: "Total", 
            revenues: 152360.7, 
            profits: 15250.0 
          },
      { rank: 13, 
           company: "ING Group", 
           revenues: 138235.3, 
           profits: 8958.9 
          },
      { rank: 14, 
            company: "Citigroup", 
            revenues: 131045.0, 
            profits: 24589.0 
          },
      { rank: 15, 
            company: "AXA", 
            revenues: 129839.2, 
            profits: 5186.5 
          },
      { rank: 16, 
            company: "Allianz", 
            revenues: 121406.0, 
            profits: 5442.4 
          },
      { rank: 17, 
            company: "Volkswagen", 
            revenues: 118376.6, 
            profits: 1391.7 
          },
      { rank: 18, 
            company: "Fortis", 
            revenues: 112351.4, 
            profits: 4896.3 
          },
      {
        rank: 19,
        company: "Crédit Agricole",
        revenues: 110764.6,
        profits: 7434.3
      },
      {
        rank: 20,
        company: "American Intl. Group",
        revenues: 108905.0,
        profits: 10477.0
      }
    ];
    let emailPattern = "[a-zA-Z0-9.\_\+\-]{1,}@[a-zA-Z0-9.-]{2,}[.]{1}[a-zA-Z]{2,}";
    this.loginForm = this.formBuilder.group({
      'email': new FormControl('',[Validators.required, Validators.email, Validators.pattern(emailPattern)]),
      'password': new FormControl('', [Validators.required, Validators.minLength(6)]),
      'check': new FormControl('', [Validators.requiredTrue]),
    });

    this.manageColumns();

  //   $('#frmt').jstree({
	// 	'core' : {
	// 		'data' : [
	// 			{
	// 				"text" : "Root node",
	// 				"state" : { "opened" : true },
	// 				"children" : [
	// 					{
	// 						"text" : "Child node 1",
	// 						"state" : { "selected" : true },
	// 						"icon" : "jstree-file"
	// 					},
	// 					{ "text" : "Child node 2", "state" : { "disabled" : true } }
	// 				]
	// 			}
	// 		]
	// 	}
	// });

  

  // setTimeout(() => {
  $(function() {
    // $('#frmt').jstree()

    $('#frmt').jstree({
      'core' : {
        'data' : [
          {
            "text" : "Root node",
            "state" : { "opened" : true },
            "children" : [
              {
                "text" : "Child node 1",
                "state" : { "selected" : true },
                "icon" : "jstree-file"
              },
              { 
                "text" : "Child node 2", 
                "state" : { "disabled" : true },
                "icon" : "jstree-file" 
              }
            ]
          }
        ]
      }
    });
  
  });
  // }, 5000);

  // debugger
  }

  public onLoginFormSubmit(values: Object): void {
    if (this.loginForm.valid) {
      // this.router.navigate(['/']);
    }
  }

  manageColumns() {
    this.columns = [
      this.manageEachColumn(
        'Rank',
        'rank',
        true,
        true,
        'number',
        false,
        null,
        { width: 100 }
      ),
      this.manageEachColumn(
        'Company',
        'company',
        true,
        false,
        'string',
        false,
        null,
        { width: 200 }
      ),
      this.manageEachColumn(
        "Revenues",
        'revenues',
        true,
        false,
        'number',
        false,
        null,
        { width: 100 }
      ),
      this.manageEachColumn(
        'Profits',
        'profits',
        true,
        false,
        'number',
        false,
        null,
        { width: 100 }
      ),
      this.manageEachColumn('Action', '', false, false, '', true, this),
    ];
  }



}
